from django.db import models


class Home(models.Model):
    home_title = models.CharField(max_length=200)
    home_text = models.CharField(max_length=600)
    pub_date = models.DateTimeField('date published')
    def __str__(self):
        return self.home_title


#class Gallery(models.Model):
#    gallery_title = models.CharField(max_length=200)
#    gallery_caption = CharField(max_length=500)
#    gallery_images = 
#    pub_date = models.DateTimeField('date published')
#    def __str__(self):
#    return self.gallery_title
#
#
#class Image(models.Model):
#    img_title = models.CharField(max_length=200)
#    img_favorite = models.Boolean(default=0)
#    pub_date = models.DateTimeField('date published')
#    img_gallery = models.ForeignKey(Gallery, on_delete=models.CASCADE)
#    def __str__(self):
#    return self.img_title
